//.c文件实现

#include "SeqList.h"

//初始化
void SLinit(SL* p1)
{
	p1->a = (SLDataType*)malloc((sizeof(SLDataType)) * 4);
	if (p1->a == NULL)
	{
		perror("malloc fail");
		return;
	}
	p1->capacity = 4;
	p1->size = 0;
}

//销毁
void SLdestory(SL* p1)
{
	free(p1->a);
	p1->a = NULL;
	p1->capacity = 0;
	p1->size = 0;
}

//扩容
void SLcheckCapcity(SL* p1)
{
	if (p1->size >= p1->capacity)
	{
		SLDataType* tmp = (SLDataType*)realloc(p1->a, sizeof(SLDataType) * p1->capacity * 2);
		if (tmp == NULL)
		{
			perror("realloc fail");
			return;
		}
		p1->a = tmp;
		p1->capacity *= 2;
	}
}

//尾插
void SLpushBack(SL* p1, SLDataType x)
{
	assert(p1);
	SLcheckCapcity(p1);//检查是否需要扩容
	p1->a[(p1->size)++] = x;//在size处插入数据
}

//打印顺序表
void SLprint(SL* p1)
{
	for (int i = 0; i < p1->size; i++)
	{
		printf("%d\n", p1->a[i]);
	}
}

//头插
void SLpushFront(SL* p1, SLDataType x)
{
	assert(p1);
	SLcheckCapcity(p1);
	for (int i = p1->size; i > 0; i--)
	{
		p1->a[i] = p1->a[i - 1];
	}
	p1->a[0] = x;
	p1->size++;
}

//尾删
void SLpopBack(SL* p1)
{
	assert(p1);
	assert(p1->size);//顺序表不为空
	//p1->a[p1->size - 1] = -1;
	p1->size--;
}

//头删
void SLpopFront(SL* p1)
{
	assert(p1);
	assert(p1->size);
	for (int i = 1; i < p1->size; i++)
	{
		p1->a[i - 1] = p1->a[i];
	}
	p1->size--;
}

//指定下标添加
void SLinsert(SL* p1, int pos, SLDataType x)
{
	//要注意p1->size指向的是最后一个有效数据的下一位
	//pos是指定的插入位置的下标(如果为0则是头插，如果为ps->size-1则为尾插)
	//x是待插入的数据
	assert(p1 && pos >= 0 && pos < p1->size);
	SLcheckCapcity(p1);
	for (int i = p1->size; i > pos; i--)
	{
		p1->a[i] = p1->a[i - 1];
	}
	p1->a[pos] = x;
	p1->size++;
}

//指定下标删除
void SLerase(SL* p1, int pos)
{
	assert(p1 && pos >= 0 && pos < p1->size);
	for (int i = pos; i < p1->size - 1; i++)
	{
		p1->a[i] = p1->a[i + 1];
	}
	p1->size--;
}

//查询
//int SLfind(SL* p1, SLDataType x)
//{
//	assert(p1);
//	for (int i = 0; i < p1->size; i++)
//	{
//		if (p1->a[i] == x)
//		{
//			return i;//找到后返回下标
//		}
//	}
//	return -1;//没有找到返回-1
//}

//int main()
//{
//	SL p;
//
//	SLinit(&p);
//
//	SLpushBack(&p, 1);
//	SLpushBack(&p, 2);
//	SLpushBack(&p, 3);
//	SLpushBack(&p, 4);
//	SLpushBack(&p, 5);
//
//	SLpushFront(&p, 6);
//	SLpushFront(&p, 7);
//	SLpushFront(&p, 8);
//
//	SLpopBack(&p);
//
//	SLpopFront(&p);
//
//	SLprint(&p);
//
//	SLdestory(&p);
//
//	return 0;
//}