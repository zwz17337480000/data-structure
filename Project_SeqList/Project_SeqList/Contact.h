#pragma once

#define NAME_MAX 20
#define GENDER_MAX 5
#define PHONE_MAX 20
#define ADDS_MAX 20

typedef struct personInfo
{
	char name[NAME_MAX];
	char gender[GENDER_MAX];
	int age;
	char phoneNum[PHONE_MAX];
	char adds[ADDS_MAX];
}peoInfo;

//前置声明
typedef struct SeqList Contact;//将顺序表命名为"通讯录"

//菜单
void menu();

//初始化
void ContactInit(Contact* p);

//销毁
void ContactDestory(Contact* p);

//添加
void ContactAdd(Contact* p);

//删除
void ContactDle(Contact* p);

//修改
void ContactModify(Contact* p);

//查找
void ContactFind(Contact* p);

//显示
void ContactShow(Contact* p);