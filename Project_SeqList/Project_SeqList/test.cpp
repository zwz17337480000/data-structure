#define _CRT_SECURE_NO_WARNINGS

#include "SeqList.h"

//test文件测试
//void TestContact()
//{
//	Contact con;//等价于SL con;
//	ContactInit(&con);
//	ContactAdd(&con);
//	/*ContactAdd(&con);
//	ContactDle(&con);*/
//	ContactModify(&con);
//	ContactFind(&con);
//	ContactShow(&con);
//	ContactDestory(&con);
//}
//
//int main()
//{
//	TestContact();
//	return 0;
//}

int main()
{
	Contact con;
	ContactInit(&con);
	while (1)
	{
		menu();
		int i = 0;
		printf("请选择你的操作：");
		scanf("%d", &i);
		switch (i)
		{
			case 1:
				ContactAdd(&con);
				break;
			case 2:
				ContactDle(&con);
				break;
			case 3:
				ContactFind(&con);
				break;
			case 4:
				ContactShow(&con);
				break;
			case 5:
				ContactModify(&con);
				break;
			case 0:
				printf("程序已退出！\n");
				break;
		}
	}
	return 0;
}