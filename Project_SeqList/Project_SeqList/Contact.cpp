

#define  _CRT_SECURE_NO_WARNINGS
//通讯录实现文件
#include "Contact.h"
#include "SeqList.h"

//菜单
void menu()
{
	printf("-----------------------\n");
	printf("        1.添加        \n");
	printf("        2.删除        \n");
	printf("        3.查找        \n");
	printf("        4.显示        \n");
	printf("        5.修改        \n");
	printf("        0.退出        \n");
	printf("-----------------------\n");
}

//初始化
void ContactInit(Contact* p)
{
	SLinit(p);
} 

//销毁
void ContactDestory(Contact* p)
{
	SLdestory(p);
}

//添加
void ContactAdd(Contact* p)
{
	peoInfo info;//联系人结构体变量

	/*int conCount;
	printf("请输入要添加的联系人数量：\n");
	scanf("%d", &conCount);*/

	/*for (int i = 1; i <= conCount; i++)
	{*/
		printf("请输入联系人的姓名：\n");
		scanf("%s", info.name);

		printf("请输入联系人的性别：\n");
		scanf("%s", info.gender);

		printf("请输入联系人的年龄：\n");
		scanf("%d", &info.age);

		printf("请输入联系人的电话号码：\n");
		scanf("%s", info.phoneNum);

		printf("请输入联系人的住址：\n");
		scanf("%s", info.adds);

		SLpushBack(p, info);//这里选择尾插

		printf("添加成功！\n\n");
	//}
}

//按名字查找
int FindName(Contact* p, char* name)
{
	for (int i = 0; i < p->size; i++)
	{
		if (strcmp(p->a[i].name, name) == 0)
			return i;//返回下标
	}
	return -1;
}

//删除(姓名)
void ContactDle(Contact* p)
{
	char n[NAME_MAX];
	printf("请输入要删除的联系人姓名：\n");
	scanf("%s", n);
	int ret = FindName(p, n);
	if (ret < 0)
	{
		printf("删除对象不存在！\n");
		return;
	}
	SLerase(p, ret);
	printf("删除成功！\n");
}

//显示
void ContactShow(Contact* p)
{
	printf("%-15s%-15s%-15s%-15s%-15s\n", "姓名", "性别", "年龄", "电话", "地址");
	for (int i = 0; i < p->size; i++)
	{
		printf("%-15s%-15s%-15d%-15s%-15s\n", p->a[i].name, p->a[i].gender, p->a[i].age,
			p->a[i].phoneNum, p->a[i].adds);
	}
}

//修改
void ContactModify(Contact* p)
{
	char name[NAME_MAX];
	printf("请输入要修改的联系人姓名：\n");
	scanf("%s", name);
	int ret = FindName(p, name);
	if (ret < 0)
	{
		printf("修改对象不存在！\n");
		return;
	}
	printf("请输入新的姓名：\n");
	scanf("%s", p->a[ret].name);

	printf("请输入新的性别：\n");
	scanf("%s", p->a[ret].gender);

	printf("请输入新的年龄：\n");
	scanf("%d", &p->a[ret].age);

	printf("请输入新的电话：\n");
	scanf("%s", p->a[ret].phoneNum);

	printf("请输入新的地址：\n");
	scanf("%s", p->a[ret].adds);

	printf("修改成功！\n\n");
}

//查找
void ContactFind(Contact* p)
{
	char name[NAME_MAX];
	printf("请输入要查找的联系人姓名：\n");
	scanf("%s", name);
	
	int ret = FindName(p, name);
	if (ret < 0)
	{
		printf("联系人不存在！\n");
		return;
	}
	printf("%-15s%-15s%-15s%-15s%-15s\n", "姓名", "性别", "年龄", "电话", "地址");
	printf("%-15s%-15s%-15d%-15s%-15s\n", p->a[ret].name, p->a[ret].gender, p->a[ret].age,
		p->a[ret].phoneNum, p->a[ret].adds);
	printf("查询成功！\n\n");
}