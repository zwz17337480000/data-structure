#pragma once
//.h文件定义
#include "Contact.h"//头文件互相包含会报错
#include <malloc.h>
#include <assert.h>
#include <string.h>
#include <stdio.h>

typedef peoInfo SLDataType;

//定义顺序表
typedef struct SeqList
{
	SLDataType* a;//数组
	int size;//有效元素
	int capacity;//容量
}SL;

//初始化
void SLinit(SL* p1);

//销毁
void SLdestory(SL* p1);

//扩容
void SLcheckCapcity(SL* p1);

//尾插
void SLpushBack(SL* p1, SLDataType x);

//打印顺序表
void SLprint(SL* p1);

//头插
void SLpushFront(SL* p1, SLDataType x);

//尾删
void SLpopBack(SL* p1);

//头删
void SLpopFront(SL* p1);

//指定插入
void SLinsert(SL* p1, int pos, SLDataType x);

//指定删除
void SLerase(SL* p1, int pos);

//查询
//int SLfind(SL* p1, SLDataType x);