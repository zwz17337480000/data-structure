#include "Queue.h"

//初始化
void QueueInit(Queue* pq)
{
	assert(pq);
	pq->head = NULL;
	pq->tail = NULL;
	pq->size = 0;
}

//创建节点
QNode* QueueBuyNode(QDataType x)
{
	QNode* newNode = (QNode*)malloc(sizeof(QNode));
	if (newNode == NULL)
	{
		perror("malloc fail!");
		exit(1);
	}
	newNode->next = NULL;
	newNode->val = x;
	return newNode;
}

//入队
void QueuePush(Queue* pq, QDataType x)
{
	assert(pq);
	QNode* node = QueueBuyNode(x);
	if (pq->head == NULL)	// 判空
	{
		pq->head = pq->tail = node;
		pq->size++;
	}
	else
	{
		pq->tail->next = node;
		pq->tail = pq->tail->next;
		pq->size++;
	}
}

//出队
void QueuePop(Queue* pq)
{
	assert(pq && pq->head);
	QNode* phead = pq->head;
	pq->head = pq->head->next;
	free(phead);
	phead = NULL;
	if (pq->head == NULL)				 //队列为空	
		pq->tail = NULL;
	pq->size--;
}

//队中元素个数
int QueueSize(Queue* pq)
{
	assert(pq);
	return pq->size;
}

//队列判空
bool QueueEmpty(Queue* pq)
{
	assert(pq);
	return pq->size == 0;
}

//队列打印
void QueuePrint(Queue* pq)
{
	assert(pq);
	if (pq->size == NULL)
	{
		printf("NULL\n");
		return;
	}
	QNode* pcur = pq->head;
	while (pcur)
	{
		printf("%d ", pcur->val);
		pcur = pcur->next;
	}
	printf("\n");
}

//取队头元素
QDataType QueueFront(Queue* pq)
{
	assert(pq && pq->head);
	return pq->head->val;
}

//取队尾元素
QDataType QueueBack(Queue* pq)
{
	assert(pq && pq->tail);
	return pq->tail->val;
}

//销毁
void QueueDestroy(Queue* pq)
{
	assert(pq);
	QNode* pcur = pq->head;
	while (pcur)
	{
		QNode* pnext = pcur->next;
		free(pcur);
		pcur = pcur->next;
	}
	pq->head = pq->tail = NULL;
	pq->size = 0;
}