#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <stdbool.h>

typedef int QDataType;

//定义队列节点
typedef struct QueueNode
{
	struct QueueNode* next;		//后继指针
	QDataType val;			//数值
}QNode;

//队列指针
typedef struct Queue
{
	QNode* head;			//队头指针
	QNode* tail;			//队尾指针
	int size;				//队列中的元素
}Queue;

//初始化
void QueueInit(Queue* pq);

//创建节点
QNode* QueueBuyNode(QDataType x);

//入队
void QueuePush(Queue* pq, QDataType x);

//出队
void QueuePop(Queue* pq);

//队中元素个数
int QueueSize(Queue* pq);

//队列判空
bool QueueEmpty(Queue* pq);

//队列打印
void QueuePrint(Queue* pq);

//取队头元素
QDataType QueueFront(Queue* pq);

//取队尾元素
QDataType QueueBack(Queue* pq);

//销毁
void QueueDestroy(Queue* pq);