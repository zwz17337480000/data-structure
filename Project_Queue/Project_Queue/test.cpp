#include "Queue.h"

void Test()
{
	Queue q;

	QueueInit(&q);

	QueuePush(&q, 1);
	QueuePush(&q, 2);
	QueuePush(&q, 3);
	QueuePush(&q, 4);
	QueuePush(&q, 5);
	QueuePop(&q);
	QueuePop(&q);
	//QueuePop(&q);
	//QueuePop(&q);
	//QueuePop(&q);

	QueuePrint(&q);
	if (QueueEmpty(&q)) printf("队列为空\n");
	else printf("队列不为空\n");
	printf("队头：%d\n队尾：%d\n队中元素：%d\n", 
		QueueFront(&q), QueueBack(&q), QueueSize(&q));
}

int main()
{
	Test();
	return 0;
}