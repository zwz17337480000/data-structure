#include <stdio.h>
#include <assert.h>
#include <stdlib.h>

typedef int HPDataType;

//堆
typedef struct Heap
{
	HPDataType* a;
	int size;
	int capacity;
}Heap;

//初始化
void HPInit(Heap* php);

//销毁
void HPDestroy(Heap* php);

//插入
void HPPush(Heap* php, HPDataType x);

//交换
void Swap(HPDataType* a, HPDataType* b);

//向上调整算法
void AdjustUP(HPDataType* a, int x);

//向下调整算法（小根堆）
void AdjustDown(HPDataType* a, int n, int child);

//删除
void HPPop(Heap* php);

//取堆顶
HPDataType HPTop(Heap* php);

//判空
bool HPEmpty(Heap* php);

//堆排序
void HPSort(HPDataType* a, int n);