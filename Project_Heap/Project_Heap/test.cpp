#define  _CRT_SECURE_NO_WARNINGS
#include "Heap.h"
#include <time.h>

void Test1()
{
	int a[] = { 2,3,1,4,6,5,9 };
	Heap hp;
	HPInit(&hp);
	for (size_t i = 0; i < sizeof(a) / sizeof(int); i++)
		HPPush(&hp, a[i]);
	int k;
	scanf_s("%d", &k);
	while (k--)
	{
		printf("%d ", HPTop(&hp));
		HPPop(&hp);
	}
	HPDestroy(&hp);
}

void Test2()
{
	int a[] = { 1,2,3,5,4,9,7,8,6 };
	HPSort(a, sizeof(a) / sizeof(int));
	for (size_t i = 0; i < sizeof(a) / sizeof(int); i++)
		printf("%d ", a[i]);
}

void CreateN()
{
	int n;
	scanf("%d", &n);
	srand((unsigned int)time(0));
	const char* FileName = "D:\\Git code\\data-structure\\Project_Heap\\Project_Heap\\data.txt";
	FILE* fin = fopen(FileName, "w");
	if (fin == NULL) {
		perror("fopen fail");
		return;
	}
	for (int i = 1; i <= n; i++) {
		int x = (rand() + i) % 10000000;
		fprintf(fin, "%d\n", x);
	}
	fclose(fin);
}

void Test3()
{
	CreateN();
	const char* FileName = "D:\\Git code\\data-structure\\Project_Heap\\Project_Heap\\data.txt";
	FILE* fout = fopen(FileName, "r");
	int k;
	scanf("%d", &k);
	int* kMinHeap = (int*)malloc(sizeof(int) * k);
	if (kMinHeap == NULL) {
		perror("malloc fail");
		return;
	}
	//将文件中的数据(前k个)读取到数组中
	for (int i = 0; i < k; i++) {
		fscanf(fout, "%d", &kMinHeap[i]);
	}
	//建堆
	for (int i = (k - 1 - 1) / 2; i >= 0; i--) {
		AdjustDown(kMinHeap, k, i);
	}
	int x;
	while (fscanf(fout, "%d", &x) > 0) {
		if (x > kMinHeap[0]) {
			kMinHeap[0] = x;
			AdjustDown(kMinHeap, k, 0);
		}
	}
	for (int i = 0; i < k; i++) {
		printf("%d\n", kMinHeap[i]);
	}
	fclose(fout);
}

int main()
{
	Test3();
	return 0;
}