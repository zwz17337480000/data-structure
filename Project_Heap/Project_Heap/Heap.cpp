#include "Heap.h"

//初始化
void HPInit(Heap* php)
{
	assert(php);
	php->a = NULL;
	php->size = php->capacity = 0;
}

//销毁
void HPDestroy(Heap* php)
{
	assert(php);
	free(php->a);
	php->a = NULL;
	php->size = php->capacity = 0;
}

//交换
void Swap(HPDataType* a, HPDataType* b)
{
	HPDataType tmp = *b;
	*b = *a;
	*a = tmp;
}

//向上调整算法
void AdjustUP(HPDataType* a, int child)
{
	int parent = (child - 1) / 2;
	while (child > 0)
	{
		if (a[parent] > a[child])
		{
			Swap(&a[parent], &a[child]);
			child = parent;
			parent = (child - 1) / 2;
		}
		else
			break;
	}
}

//向下调整算法
void AdjustDown(HPDataType* a, int n, int parent)
{
	int child = 2 * parent + 1;
	while (child < n)
	{
		if (child + 1 < n && a[child] > a[child + 1])
			child++;
		if (a[parent] > a[child])
		{
			Swap(&a[parent], &a[child]);
			parent = child;
			child = 2 * parent + 1;
		}
		else
			break;
	}
}

//插入
void HPPush(Heap* php, HPDataType x)
{
	assert(php);
	if (php->size == php->capacity)
	{
		int newcapacity = php->capacity == 0 ? 4 : php->capacity * 2;
		HPDataType* tmp = (HPDataType*)realloc(php->a, newcapacity * sizeof(HPDataType));
		if (tmp == NULL)
		{
			perror("realloc fail!");
			return;
		}
		php->a = tmp;
		php->capacity = newcapacity;
	}
	php->a[php->size++] = x;
	AdjustUP(php->a, php->size - 1);
}

//删除（删除堆顶的数据）
void HPPop(Heap* php)
{
	assert(php && php->size > 0);
	Swap(&php->a[0], &php->a[php->size - 1]);
	php->size--;
	AdjustDown(php->a, php->size, 0);
}

//取堆顶
HPDataType HPTop(Heap* php)
{
	assert(php && php->size > 0);
	return php->a[0];
}

//判空
bool HPEmpty(Heap* php)
{
	assert(php);
	return php->size == 0;
}

//堆排序(O(N*logN))
void HPSort(HPDataType* a, int n)
{
	//降序：建小堆
	//升序：建大堆
	//for (int i = 1; i < n; i++)
	//	AdjustUP(a, i);//向上调整建堆
	for (int i = (n - 1 - 1) / 2; i < n; i++)
		AdjustDown(a, n, i);
	int end = n - 1;
	while (end > 0)
	{
		Swap(&a[0], &a[end]);
		AdjustDown(a, end, 0);
		--end;
	}
}