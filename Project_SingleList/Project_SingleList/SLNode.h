#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

typedef int SLTDataType;

//定义节点
typedef struct SLNode
{
	SLTDataType data;//数据域
	struct SLNode* next;//指针域
}SLTNode;

//打印
void SLTprint(SLTNode* phead);//phead是指向链表第一个节点的指针

//尾插
void SLTpushBack(SLTNode** pphead, SLTDataType x);

//头插
void SLTpushFront(SLTNode** pphead, SLTDataType x);

//尾删
void SLTpopBack(SLTNode** pphead);

//头删
void SLTpopFront(SLTNode** pphead);

//查找
SLNode* SLTfind(SLTNode* phead, SLTDataType x);

//在指定位置(pos)之前插入数据
void SLTinsert(SLTNode** pphead, SLTNode* pos, SLTDataType x);

//在指定位置(pos)之后插入数据
void SLTinsertAfter(SLTNode* pos, SLTDataType x);

//删除pos节点
void SLTerase(SLTNode** pphead, SLTNode* pos);

//删除pos之后的节点
void SLTeraseAfter(SLTNode* pos);

//销毁链表
void SLTdestroy(SLTNode** pphead);