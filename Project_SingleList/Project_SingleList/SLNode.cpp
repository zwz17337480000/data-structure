#include "SLNode.h"

//打印
void SLTprint(SLTNode* phead)
{
	SLNode* pcur = phead;
	while (pcur != NULL)
	{
		printf("%d ", pcur->data);
		pcur = pcur->next;
	}
	printf("\n");
}

//创建节点并初始化
SLNode* SLTbuyNode(SLTDataType x)
{
	SLNode* newnode = (SLNode*)malloc(sizeof(SLNode));//创建新节点
	if (newnode == NULL)
	{
		perror("malloc fail!");
		exit(1);//表示非正常退出
	}
	newnode->data = x;
	newnode->next = NULL;
	return newnode;
}

//尾插
void SLTpushBack(SLTNode** pphead, SLTDataType x)
{
	assert(pphead);
	SLTNode* newnode = SLTbuyNode(x);
	if (*pphead == NULL)//链表为空
	{
		*pphead = newnode;
	}
	else
	{
		SLNode* ptail = *pphead;
		while (ptail->next != NULL)//遍历链表找到尾节点
		{
			ptail = ptail->next;
		}
		ptail->next = newnode;
	}
}

//头插
void SLTpushFront(SLTNode** pphead, SLTDataType x)
{
	assert(pphead);
	SLTNode* newnode = SLTbuyNode(x);
	newnode->next = *pphead;//*pphead是指向第一个节点的指针
	*pphead = newnode;
}

//尾删
void SLTpopBack(SLTNode** pphead)
{
	assert(pphead && *pphead);//*pphead为空说明整个链表为空
	if ((*pphead)->next == NULL)//链表中只有一个节点
	{
		free(*pphead);
		*pphead = NULL;
	}
	else
	{
		SLTNode* ptail = *pphead;
		SLTNode* prev = *pphead;
		while (ptail->next != NULL)
		{
			prev = ptail;//prev指向的是尾节点的前一个节点
			ptail = ptail->next;
		}
		free(ptail);
		prev->next = NULL;//prev成为新的尾节点
		ptail = NULL;
	}
}

//头删
void SLTpopFront(SLTNode** pphead)
{
	assert(pphead && *pphead);
	if ((*pphead) == NULL)
	{
		free(*pphead);
		*pphead = NULL;
	}
	else
	{
		SLTNode* p = *pphead;//此时p指向的是头节点
		*pphead = (*pphead)->next;
		free(p);
		p = NULL;
	}
}

//查找(返回节点)
SLNode* SLTfind(SLTNode* phead, SLTDataType x)
{
	assert(phead);
	SLNode* pcur = phead;
	while (pcur != NULL)
	{
		if (pcur->data == x)
		{
			return pcur;
		}
		pcur = pcur->next;
	}
	return NULL;
}

//在指定位置(pos)之前插入数据
void SLTinsert(SLTNode** pphead, SLTNode* pos, SLTDataType x)
{
	assert(pphead && *pphead && pos);
	SLTNode* pcur = *pphead;
	SLNode* newnode = SLTbuyNode(x);
	if (pos == *pphead)
	{
		SLTpushFront(pphead, x);
	}
	else
	{
		while (pcur->next != pos)//遍历到pos节点的前驱节点
		{
			pcur = pcur->next;
		}
		newnode->next = pos;
		pcur->next = newnode;
	}
}

//在指定位置(pos)之后插入数据
void SLTinsertAfter(SLTNode* pos, SLTDataType x)
{
	assert(pos);
	SLNode* newnode = SLTbuyNode(x);
	if (pos->next == NULL)//如果pos是尾节点
	{
		pos->next = newnode;
		newnode->next = NULL;
	}
	else
	{
		SLNode* pafter = pos->next;//pcur是pos的后继节点
		newnode->next = pafter;
		pos->next = newnode;
	}
}

//删除pos节点
void SLTerase(SLTNode** pphead, SLTNode* pos)
{
	assert(*pphead && pos && pphead);
	if (pos->next == NULL)//如果pos是尾节点
	{
		SLTNode* prev = *pphead;
		while (prev->next != pos)
		{
			prev = prev->next;
		}
		prev->next = NULL;
		free(pos);
		pos = NULL;
	}
	else if (*pphead == pos)//如果pos是头节点
	{
		SLTNode* next = (*pphead)->next;
		free(*pphead);
		(*pphead) = next;
	}
	else
	{
		SLTNode* prev = *pphead;
		while (prev->next != pos)
		{
			prev = prev->next;
		}
		prev->next = pos->next;
		free(pos);
		pos = NULL;
	}
}

//删除pos之后的节点
void SLTeraseAfter(SLTNode* pos)
{
	assert(pos->next && pos);
	SLTNode* next = pos->next;
	pos->next = pos->next->next;
	free(next);
	next = NULL;
}

//销毁链表
void SLTdestroy(SLTNode** pphead)
{
	assert(*pphead && pphead);
	SLTNode* pcur = *pphead;
	while (pcur != NULL)
	{
		SLTNode* next = pcur->next;
		free(pcur);
		pcur = next;
	}
	*pphead = NULL;
}