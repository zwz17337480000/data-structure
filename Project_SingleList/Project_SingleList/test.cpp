#include "SLNode.h"

//测试1
void Test1()
{
	//创建节点
	SLTNode* node1 = (SLTNode*)malloc(sizeof(SLTNode));
	node1->data = 1;

	SLTNode* node2 = (SLTNode*)malloc(sizeof(SLTNode));
	node2->data = 2;

	SLTNode* node3 = (SLTNode*)malloc(sizeof(SLTNode));
	node3->data = 3;

	SLTNode* node4 = (SLTNode*)malloc(sizeof(SLTNode));
	node4->data = 4;

	//链接节点
	node1->next = node2;
	node2->next = node3;
	node3->next = node4;
	node4->next = NULL;//尾指针置空

	//打印
	SLNode* plist = node1;
	SLNode* ptmp = node2;
	/*SLTinsert(&plist, ptmp, 9);
	SLTinsert(&plist, plist, 9);*/
	SLTinsertAfter(node4, 9);
	SLTinsertAfter(node2, 9);
	SLTerase(&plist, ptmp);
	SLTerase(&plist, node4);
	//SLTerase(&plist, node1);

	SLTprint(plist);
}

//测试2
void Test2()
{
	SLNode* plist = NULL;

	//尾插
	SLTpushBack(&plist, 1);
	SLTpushBack(&plist, 2);
	SLTpushBack(&plist, 3);
	SLTpushBack(&plist, 4);
	SLTprint(plist);

	//头插
	/*SLTpushFront(&plist, 6);
	SLTpushFront(&plist, 7);
	SLTpushFront(&plist, 8);
	SLTprint(plist);*/

	//尾删
	/*SLTpopBack(&plist);
	SLTpopBack(&plist);
	SLTpopBack(&plist);
	SLTprint(plist);*/

	//头删
	/*SLTpopFront(&plist);
	SLTpopFront(&plist);
	SLTpopFront(&plist);
	SLTprint(plist);*/

	//查找
	SLTNode* ptmp = SLTfind(plist, 1);
	/*if (ptmp == NULL)
	{
		printf("没有找到！\n");
		return;
	}
	else
	{
		printf("找到了%d", ptmp->data);
	}*/

	//SLTeraseAfter(ptmp);
	SLTdestroy(&plist);
	SLTprint(plist);
}

int main()
{
	//Test1();
	Test2();
	return 0;
}