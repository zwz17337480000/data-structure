#include <stdio.h>
#include <malloc.h>
#include <Windows.h>
#include <assert.h>
#include "Queue.h"

typedef char BTDataType;
typedef struct BinaryTreeNode {
	BTDataType data;
	struct BinaryTreeNode* left;  // 左子树根节点
	struct BinaryTreeNode* right; // 右子树根节点
}BTNode;

// 创建节点
BTNode* BuyNode(BTDataType x) {
	BTNode* node = (BTNode*)malloc(sizeof(node));
	if (node == NULL) {
		perror("malloc fail!");
		exit(-1);
	}
	node->data = x;
	node->left = NULL;
	node->right = NULL;
	return node;
}

// 创建二叉树(数组创建)
BTNode* CreateTree(BTDataType* arr, int* pi) {
	if (arr[*pi] == '#') {
		(*pi)++;
		return NULL;
	}
	BTNode* node = (BTNode*)malloc(sizeof(BTNode));
	if (node == NULL) {
		perror("malloc fail!");
		exit(-1);
	}
	node->data = arr[(*pi)++];
	node->left = CreateTree(arr, pi);
	node->right = CreateTree(arr, pi);
	return node;
}

// 销毁二叉树
void TreeDestory(BTNode* root) {
	if (root == NULL) {
		return;
	}

	// 采用后序遍历销毁树
	TreeDestory(root->left);
	TreeDestory(root->right);
	free(root);
}

// 层序遍历
void TreeLevelOrder(BTNode* root) {
	Queue q;
	QueueInit(&q);

	// 不为空进队列
	if (root) {
		QueuePush(&q, root);
	}

	// 如果队列是空的，代表已经访问完所有元素
	while (!QueueEmpty(&q)) {
		BTNode* front = QueueFront(&q);
		QueuePop(&q);
		printf("%c ", front->data);

		if (front->left) {
			QueuePush(&q, front->left);
		}

		if (front->right) {
			QueuePush(&q, front->right);
		}
	}
	QueueDestroy(&q);
}

// 创建树
//BTNode* CreateBinaryTree()
//{
//	BTNode* node1 = BuyNode(1);
//	BTNode* node2 = BuyNode(2);
//	BTNode* node3 = BuyNode(3);
//	BTNode* node4 = BuyNode(4);
//	BTNode* node5 = BuyNode(5);
//	BTNode* node6 = BuyNode(6);
//
//	node1->left = node2;
//	node1->right = node4;
//	node2->left = node3;
//	node4->left = node5;
//	node4->right = node6;
//	return node1;
//}

// 前序遍历
void PrevOrder(BTNode* root) {
	if (root == NULL) {
		printf("N ");
		return;
	}
	printf("%d ", root->data);
	PrevOrder(root->left);
	PrevOrder(root->right);
}

// 中序遍历
void InOrder(BTNode* root) {
	if (root == NULL) {
		printf("N ");
		return;
	}
	InOrder(root->left);
	printf("%d ", root->data);
	InOrder(root->right);
}

// 后序遍历
void PostOrder(BTNode* root) {
	if (root == NULL) {
		printf("N ");
		return;
	}
	PostOrder(root->left);
	PostOrder(root->right);
	printf("%d ", root->data);
}

// 节点数
// 写法一
//static int size = 0;
//int TreeSize(BTNode* root) {
//	if (root == NULL) {
//		return 0;
//	}
//	else {
//		size++;
//	}
//
//	TreeSize(root->left);
//	TreeSize(root->right);
//	return size;
//}

// 写法二
//int TreeSize(BTNode* root, int* psize) {
//	if (root == NULL) {
//		return 0;
//	}
//	else {
//		(*psize)++;
//	}
//
//	TreeSize(root->left, psize);
//	TreeSize(root->right, psize);
//}

// 节点数：写法三
int TreeSize(BTNode* root) {
	return root == NULL ? 0 :
		TreeSize(root->left) + TreeSize(root->right) + 1;
}

// 叶子节点数
int TreeLeafSize(BTNode* root) {
	if (root == NULL) {
		return 0;
	}
	else if (root->left == NULL && root->right == NULL) {
		return 1;
	}
	else {
		return TreeLeafSize(root->left) + TreeLeafSize(root->right);
	}
}

// 树的高度
int TreeHeight(BTNode* root) {
	if (root == NULL) {
		return 0;
	}
	else {
		return max(TreeHeight(root->left), TreeHeight(root->right)) + 1;
	}
}

// 求第k层节点数
int TreeLevelKSize(BTNode* root, int k) {
	if (root == NULL) {
		return 0;
	}

	if (k == 1) {
		return 1;
	}
	else {
		return TreeLevelKSize(root->left, k - 1) + TreeLevelKSize(root->right, k - 1);
	}
}

// 查找值为x的节点
BTNode* TreeFind(BTNode* root, BTDataType x) {
	if (root == NULL) {
		return NULL;
	}

	if (root->data == x) {
		return root;
	}
	BTNode* ptr = TreeFind(root->left, x);
	if (ptr != NULL) {
		return ptr;
	}
	else {
		BTNode* ptr = TreeFind(root->right, x);
		if (ptr == NULL) {
			return NULL;
		}
		else {
			return ptr;
		}
	}
}

// 判断完全二叉树
//bool BinaryTreeComplete(BTNode* root) {
//	Queue q;
//	QueueInit(&q);
//
//	if (root) {
//		QueuePush(&q, root);
//	}
//
//	while (!QueueEmpty(&q)) {
//		BTNode* front = QueueFront(&q);
//		QueuePop(&q);
//
//		// 遇到第一个空开始检查队列
//		if (front == NULL) {
//			break;
//		}
//		
//		// 无论是否为空都进队列
//		QueuePush(&q, front->left);
//		QueuePush(&q, front->right);
//	}
//
//	// 检查队列
//	while (!QueueEmpty(&q)) {
//		BTNode* front = QueueFront(&q);
//		QueuePop(&q);
//
//		if (front != NULL) {
//			QueueDestroy(&q);
//			return false;
//		}
//	}
//	
//	QueueDestroy(&q);
//	return true;
//}

bool TreeComplete(BTNode* root)
{
	Queue q;
	QueueInit(&q);
	if (root)
		QueuePush(&q, root);

	while (!QueueEmpty(&q))
	{
		BTNode* front = QueueFront(&q);
		QueuePop(&q);

		// 遇到第一个空，就可以开始判断，如果队列中还有非空，就不是完全二叉树
		if (front == NULL)
		{
			break;
		}

		QueuePush(&q, front->left);
		QueuePush(&q, front->right);
	}

	while (!QueueEmpty(&q))
	{
		BTNode* front = QueueFront(&q);
		QueuePop(&q);

		// 如果有非空，就不是完全二叉树
		if (front)
		{
			QueueDestroy(&q);
			return false;
		}
	}

	QueueDestroy(&q);
	return true;
}

BTNode* TestCreatBinaryTree()
{
	BTNode* node1 = BuyNode(1);
	BTNode* node2 = BuyNode(2);
	BTNode* node3 = BuyNode(3);
	BTNode* node4 = BuyNode(4);
	BTNode* node5 = BuyNode(5);
	BTNode* node6 = BuyNode(6);
	BTNode* node7 = BuyNode(7);


	node1->left = node2;
	node1->right = node4;
	node2->left = node3;
	node2->right = NULL;
	node3->left = NULL;
	node3->right = NULL;
	node4->left = node5;
	node4->right = node6;
	node5->right = node7;
	node5->left = NULL;
	node6->left = node6->right = NULL;
	node7->left = node7->right = NULL;

	return node1;
}

int main() {
	//BTNode* root = CreateBinaryTree();

	/*PrevOrder(root);
	printf("\n");
	InOrder(root);
	printf("\n");
	PostOrder(root);*/

	// 写法一
	/*printf("TreeSize:%d\n", TreeSize(root));
	size = 0;
	printf("TreeSize:%d\n", TreeSize(root));*/

	// 写法二
	/*int size = 0;
	TreeSize(root, &size);
	printf("TreeSize:%d\n", size);
	printf("TreeSize:%d\n", size);*/

	// 写法三
	/*printf("TreeSize:%d\n", TreeSize(root));
	printf("TreeSize:%d\n", TreeSize(root));*/

	/*printf("TreeLeafSize:%d\n", TreeLeafSize(root));
	printf("TreeLeafSize:%d\n", TreeLeafSize(root));*/

	/*printf("TreeHeight:%d\n", TreeHeight(root));
	printf("TreeHeight:%d\n", TreeHeight(root));*/

	/*printf("TreeLevelKSize:%d\n", TreeLevelKSize(root, 3));
	printf("TreeLevelKSize:%d\n", TreeLevelKSize(root, 3));*/

	/*BTNode* btp = TreeFind(root, 1);
	printf("TreeFind:%d\n", btp->data);
	printf("TreeFind:%d\n", btp->data);*/

	/*char a[] = { "abc##de#g##f###" };
	int i = 0;
	BTNode* root = CreateTree(a, &i);*/
	/*PrevOrder(root);*/

	/*TreeLevelOrder(root);*/

	BTNode* root = TestCreatBinaryTree();

	if (TreeComplete(root)) {
		printf("这棵树是完全二叉树\n");
	}
	else {
		printf("这棵树是非完全二叉树\n");
	}

	PrevOrder(root);

	return 0;
}